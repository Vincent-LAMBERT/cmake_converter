# utils file for projects came from visual studio solution with cmake-converter.

################################################################################
# Wrap each token of the command with condition
################################################################################
cmake_policy(PUSH)
cmake_policy(SET CMP0054 NEW)
macro(prepare_commands)
    unset(TOKEN_ROLE)
    unset(COMMANDS)
    foreach(TOKEN ${ARG_COMMANDS})
        if("${TOKEN}" STREQUAL "COMMAND")
            set(TOKEN_ROLE "KEYWORD")
        elseif("${TOKEN_ROLE}" STREQUAL "KEYWORD")
            set(TOKEN_ROLE "CONDITION")
        elseif("${TOKEN_ROLE}" STREQUAL "CONDITION")
            set(TOKEN_ROLE "COMMAND")
        elseif("${TOKEN_ROLE}" STREQUAL "COMMAND")
            set(TOKEN_ROLE "ARG")
        endif()

        if("${TOKEN_ROLE}" STREQUAL "KEYWORD")
            list(APPEND COMMANDS "${TOKEN}")
        elseif("${TOKEN_ROLE}" STREQUAL "CONDITION")
            set(CONDITION ${TOKEN})
        elseif("${TOKEN_ROLE}" STREQUAL "COMMAND")
            list(APPEND COMMANDS "$<$<NOT:${CONDITION}>:${DUMMY}>$<${CONDITION}:${TOKEN}>")
        elseif("${TOKEN_ROLE}" STREQUAL "ARG")
            list(APPEND COMMANDS "$<${CONDITION}:${TOKEN}>")
        endif()
    endforeach()
endmacro()
cmake_policy(POP)

################################################################################
# Transform all the tokens to absolute paths
################################################################################
macro(prepare_output)
    unset(OUTPUT)
    foreach(TOKEN ${ARG_OUTPUT})
        if(IS_ABSOLUTE ${TOKEN})
            list(APPEND OUTPUT "${TOKEN}")
        else()
            list(APPEND OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/${TOKEN}")
        endif()
    endforeach()
endmacro()

################################################################################
# Parse add_custom_command_if args.
#
# Input:
#     PRE_BUILD  - Pre build event option
#     PRE_LINK   - Pre link event option
#     POST_BUILD - Post build event option
#     TARGET     - Target
#     OUTPUT     - List of output files
#     DEPENDS    - List of files on which the command depends
#     COMMANDS   - List of commands(COMMAND condition1 commannd1 args1 COMMAND
#                  condition2 commannd2 args2 ...)
# Output:
#     OUTPUT     - Output files
#     DEPENDS    - Files on which the command depends
#     COMMENT    - Comment
#     PRE_BUILD  - TRUE/FALSE
#     PRE_LINK   - TRUE/FALSE
#     POST_BUILD - TRUE/FALSE
#     TARGET     - Target name
#     COMMANDS   - Prepared commands(every token is wrapped in CONDITION)
#     NAME       - Unique name for custom target
#     STEP       - PRE_BUILD/PRE_LINK/POST_BUILD
################################################################################
function(add_custom_command_if_parse_arguments)
    cmake_parse_arguments("ARG" "PRE_BUILD;PRE_LINK;POST_BUILD" "TARGET;COMMENT" "DEPENDS;OUTPUT;COMMANDS" ${ARGN})

    if(WIN32)
        set(DUMMY "cd.")
    elseif(UNIX)
        set(DUMMY "true")
    endif()

    prepare_commands()
    prepare_output()

    set(DEPENDS "${ARG_DEPENDS}")
    set(COMMENT "${ARG_COMMENT}")
    set(PRE_BUILD "${ARG_PRE_BUILD}")
    set(PRE_LINK "${ARG_PRE_LINK}")
    set(POST_BUILD "${ARG_POST_BUILD}")
    set(TARGET "${ARG_TARGET}")
    if(PRE_BUILD)
        set(STEP "PRE_BUILD")
    elseif(PRE_LINK)
        set(STEP "PRE_LINK")
    elseif(POST_BUILD)
        set(STEP "POST_BUILD")
    endif()
    set(NAME "${TARGET}_${STEP}")

    set(OUTPUT "${OUTPUT}" PARENT_SCOPE)
    set(DEPENDS "${DEPENDS}" PARENT_SCOPE)
    set(COMMENT "${COMMENT}" PARENT_SCOPE)
    set(PRE_BUILD "${PRE_BUILD}" PARENT_SCOPE)
    set(PRE_LINK "${PRE_LINK}" PARENT_SCOPE)
    set(POST_BUILD "${POST_BUILD}" PARENT_SCOPE)
    set(TARGET "${TARGET}" PARENT_SCOPE)
    set(COMMANDS "${COMMANDS}" PARENT_SCOPE)
    set(STEP "${STEP}" PARENT_SCOPE)
    set(NAME "${NAME}" PARENT_SCOPE)
endfunction()

################################################################################
# Add conditional custom command
#
# Generating Files
# The first signature is for adding a custom command to produce an output:
#     add_custom_command_if(
#         <OUTPUT output1 [output2 ...]>
#         <COMMANDS>
#         <COMMAND condition command1 [args1...]>
#         [COMMAND condition command2 [args2...]]
#         [DEPENDS [depends...]]
#         [COMMENT comment]
#
# Build Events
#     add_custom_command_if(
#         <TARGET target>
#         <PRE_BUILD | PRE_LINK | POST_BUILD>
#         <COMMAND condition command1 [args1...]>
#         [COMMAND condition command2 [args2...]]
#         [COMMENT comment]
#
# Input:
#     output     - Output files the command is expected to produce
#     condition  - Generator expression for wrapping the command
#     command    - Command-line(s) to execute at build time.
#     args       - Command`s args
#     depends    - Files on which the command depends
#     comment    - Display the given message before the commands are executed at
#                  build time.
#     PRE_BUILD  - Run before any other rules are executed within the target
#     PRE_LINK   - Run after sources have been compiled but before linking the
#                  binary
#     POST_BUILD - Run after all other rules within the target have been
#                  executed
################################################################################
function(add_custom_command_if)
    add_custom_command_if_parse_arguments(${ARGN})

    if(OUTPUT AND TARGET)
        message(FATAL_ERROR  "Wrong syntax. A TARGET and OUTPUT can not both be specified.")
    endif()

    if(OUTPUT)
        add_custom_command(OUTPUT ${OUTPUT}
                           ${COMMANDS}
                           DEPENDS ${DEPENDS}
                           WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                           COMMENT ${COMMENT})
    elseif(TARGET)
        if(PRE_BUILD AND NOT ${CMAKE_GENERATOR} MATCHES "Visual Studio")
            add_custom_target(
                ${NAME}
                ${COMMANDS}
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                COMMENT ${COMMENT})
            add_dependencies(${TARGET} ${NAME})
        else()
            add_custom_command(
                TARGET ${TARGET}
                ${STEP}
                ${COMMANDS}
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                COMMENT ${COMMENT})
        endif()
    else()
        message(FATAL_ERROR "Wrong syntax. A TARGET or OUTPUT must be specified.")
    endif()
endfunction()

# 0x0010 Name changed from use_props to use_props_with_config
################################################################################
# Use props file for a target and configs
#     use_props_with_config(<target> <configs...> <props_file>)
# Inside <props_file> there are following variables:
#     PROPS_TARGET   - <target>
#     PROPS_CONFIG   - One of <configs...>
#     PROPS_CONFIG_U - Uppercase PROPS_CONFIG
# Input:
#     target      - Target to apply props file
#     configs     - Build configurations to apply props file
#     props_file  - CMake script
################################################################################
macro(use_props TARGET PROPS_FILE)
    set(PROPS_TARGET "${TARGET}")
    foreach(PROPS_CONFIG ${CONFIGS})
        string(TOUPPER "${PROPS_CONFIG}" PROPS_CONFIG_U)

        get_filename_component(ABSOLUTE_PROPS_FILE "${PROPS_FILE}" ABSOLUTE BASE_DIR "${CMAKE_CURRENT_LIST_DIR}")
        if(EXISTS "${ABSOLUTE_PROPS_FILE}")
            message("${ABSOLUTE_PROPS_FILE}")
            include("${ABSOLUTE_PROPS_FILE}")
        else()
            message(WARNING "Corresponding cmake file from props \"${ABSOLUTE_PROPS_FILE}\" doesn't exist")
        endif()
    endforeach()
endmacro()


# 0x0010 The new default use_props which does not give the target and config
# to the existing .cmake but only includes it if the given config is in the 
# current ones. Plus, does not pass TARGET as argument for clarity issues in
# the .cmake done by hand 
################################################################################
# Use props file if one of the given configs is in the current ones
#     use_props_if_config(<target> <configs...> <props_file>)
# Input:
#     configs     - Build configurations to apply props file
#     props_file  - CMake script
################################################################################
function(use_props_if_config TARGET CONFIGS PROPS_FILE)
    set(PROPS_TARGET "${TARGET}")
    get_filename_component(ABSOLUTE_PROPS_FILE "${PROPS_FILE}" ABSOLUTE BASE_DIR "${CMAKE_CURRENT_LIST_DIR}")
    if(EXISTS "${ABSOLUTE_PROPS_FILE}")
        foreach(PROPS_CONFIG ${CONFIGS})
            set(STR_LIST ${CMAKE_CONFIGURATION_TYPES})
            foreach(CURRENT_CONFIG ${CMAKE_CONFIGURATION_TYPES})
                if ("${PROPS_CONFIG}" STREQUAL "${CURRENT_CONFIG}")
                    message("${ABSOLUTE_PROPS_FILE}")
                    include("${ABSOLUTE_PROPS_FILE}")
                    return()
                endif()
            endforeach()
        endforeach()
    else()
        message(WARNING "Corresponding cmake file from props \"${ABSOLUTE_PROPS_FILE}\" doesn't exist")
    endif()
endfunction()



################################################################################
# Add compile options to source file
#     source_file_compile_options(<source_file> [compile_options...])
# Input:
#     source_file     - Source file
#     compile_options - Options to add to COMPILE_FLAGS property
################################################################################
function(source_file_compile_options SOURCE_FILE)
    if("${ARGC}" LESS_EQUAL "1")
        return()
    endif()

    get_source_file_property(COMPILE_OPTIONS "${SOURCE_FILE}" COMPILE_OPTIONS)

    if(COMPILE_OPTIONS)
        list(APPEND COMPILE_OPTIONS ${ARGN})
    else()
        set(COMPILE_OPTIONS "${ARGN}")
    endif()

    set_source_files_properties("${SOURCE_FILE}" PROPERTIES COMPILE_OPTIONS "${COMPILE_OPTIONS}")
endfunction()



# 0x001B Changing add_precompiled_header function which bugued with the IDE
# and didn't produce the expected file
################################################################################
# Function for MSVC precompiled headers
#     add_precompiled_header(<target> <precompiled_header> <precompiled_source>)
# Input:
#     target             - Target to which add precompiled header
#     precompiled_header - Name of precompiled header
#     precompiled_source - Name of precompiled source file
################################################################################
function(target_custom_add_precompiled_header TARGET PRECOMPILED_HEADER PRECOMPILED_SOURCE)
    set(PRECOMPILED_BINARY "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pch")
    string(REPLACE "/" "\\" PRECOMPILED_BINARY_WIN ${PRECOMPILED_BINARY})

    if (MSVC)
        set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "/Yu${PRECOMPILED_HEADER} /Fp${PRECOMPILED_BINARY_WIN}")
        set_source_files_properties(${PRECOMPILED_SOURCE} PROPERTIES COMPILE_FLAGS "/Yc${PRECOMPILED_HEADER} /Fp${PRECOMPILED_BINARY_WIN}")
    endif()
endfunction()

# 0x001A Adding target_remove_definitions because remove_definitions removes only
# those added by add_definitions
################################################################################
# Removes definitions for a given target
#     add_precompiled_header(<target> <regex-definition>)
# Input:
#     target             - Target to which remove definition
#     regex-definition   - definition in regex style (ex: [[MY_DEFINITION]])
################################################################################
function(target_remove_definitions TARGET DEFINITIONS)
    foreach(DEFINITION ${DEFINITIONS}) 
        get_target_property(DEFS ${PROJECT_NAME} COMPILE_DEFINITIONS)
        list(FILTER DEFS EXCLUDE REGEX ${DEFINITION})
        set_property(TARGET ${PROJECT_NAME} PROPERTY COMPILE_DEFINITIONS ${DEFS})
    endforeach()
endfunction()

# 0x0018 Correcting ExcludedFromBuild behavior to handle
# configuration-dependant cases
################################################################################
# Exclude file from build in special configurations
# Outputs the filename or nothing in a variable with filename shape replacing "."
# and "/" with "_" 
#     exclude_from_build(<source_file> <config_platform> [...])
# Input:
#     source_file     - Source file
#     config_platform - String with "<config>|<platform>" shape
################################################################################
function(exclude_from_build FILENAME)
    cmake_parse_arguments("ARG" "" "" "CONFIGS" ${ARGN})
    if ("${ARG_CONFIGS}" STREQUAL "")
        message(FATAL_ERROR "The config platforms cannot be empty!")
    endif()
    set(NOT_EXCLUDED TRUE)
    foreach(CONFIG_PLATFORM ${ARG_CONFIGS})
        string(CONCAT CURRENT_CONFIG_PLATFORM "${CMAKE_CONFIGURATION_TYPES}" "|" "${CMAKE_VS_PLATFORM_NAME}")
        if ("${CURRENT_CONFIG_PLATFORM}" STREQUAL "${CONFIG_PLATFORM}")
            set(NOT_EXCLUDED FALSE)
        endif()
    endforeach()
    string(REPLACE "/" "_" TEMP ${FILENAME})
    string(REPLACE "." "_" OUT_NAME ${TEMP})
    set(VALUE "${OUT_NAME}")
    if (${NOT_EXCLUDED})
        set(${OUT_NAME} ${FILENAME} PARENT_SCOPE)
    else()
        set(${OUT_NAME} "" PARENT_SCOPE)
    endif()
endfunction()

# 0x000A Adding replace function for environment variables
################################################################################
# Replaces environment variable parts into another
#       replace_environment_variable(<match-string> <replace-string> 
#               <out-var-name> <input-var-name> <input-str>)
# Input:
#       match-string    - String to replace in <input-var-name> eponym variable
#       replace-string  - String to put instead of match-string
#       out-var-name    - Name of the output var
#       input-var-name  - Name of the input variable as str
#       input-str       - String in which apply replace on input-var
################################################################################
function(replace_environment_variable)
    cmake_parse_arguments("ARG" "" "STR_TO_REPLACE;STR_TO_PUT;OUTPUT_VAR_NAME;VAR_TO_REPLACE_NAME" "INPUT_STR" ${ARGN})

    if ("${ARG_STR_TO_REPLACE}" STREQUAL "")
        message(FATAL_ERROR "The replaced string cannot be empty!")
    endif()
    if ("${ARG_OUTPUT_VAR_NAME}" STREQUAL "")
        message(FATAL_ERROR "The output variable name cannot be empty!")
    endif()
    if ("${ARG_VAR_TO_REPLACE_NAME}" STREQUAL "")
        message(FATAL_ERROR "The variable to replace name cannot be empty!")
    else()
        if ("$ENV{${ARG_VAR_TO_REPLACE_NAME}}" STREQUAL "")
            message(FATAL_ERROR "The variable to replace cannot be empty!")
        endif()
    endif()
    string(REPLACE ${ARG_STR_TO_REPLACE} "${ARG_STR_TO_PUT}" VAR $ENV{${ARG_VAR_TO_REPLACE_NAME}})
    string(JOIN ${VAR} ${ARG_OUTPUT_VAR_NAME} ${ARG_INPUT_STR})
    set(${ARG_OUTPUT_VAR_NAME} ${${ARG_OUTPUT_VAR_NAME}} PARENT_SCOPE)
endfunction()

#0x000C Adding lowering function for environment variables
################################################################################
# Converts the content of the given variable to lowercase
#     lower_environment_variable(<variable>)
# Input:
#     variable     - variable on which to apply lowercase function
################################################################################
function(lower_environment_variable VAR_NAME)
    if(NOT("${ARGC}" EQUAL "1"))
        return()
    endif()
    if ("${VAR_NAME}" STREQUAL "")
        message(FATAL_ERROR "The variable name cannot be empty!")
        if ("$ENV{${VAR_NAME}}" STREQUAL "")
            message(FATAL_ERROR "The variable cannot be empty!")
        endif()
    endif()
    string(TOLOWER $ENV{${VAR_NAME}} NEW_VAR)
    set(ENV{${VAR_NAME}} ${NEW_VAR})
endfunction()

#0x0011 Adding macro to debug
################################################################################
# Prints the compiler flags used for the current project
#     print_compile_flags()
################################################################################
macro(print_compile_flags)
    get_target_property(MAIN_CFLAGS ${PROJECT_NAME} COMPILE_OPTIONS)
    # also see: COMPILE_DEFINITIONS INCLUDE_DIRECTORIES
    message("-- Target compiler flags are: ${MAIN_CFLAGS}")
endmacro()

#0x0015 Adding macro to debug
################################################################################
# Prints the given property used for the current project
#     print_property(PROP)
################################################################################
macro(print_property PROP_NAME)
    message("${PROP_NAME}")
    get_target_property(PROPERTY ${PROJECT_NAME} ${PROP_NAME})
    # also see: LINK_DIRECTORIES
    message("-- The ${PROP_NAME} property is : ${PROPERTY}")
endmacro()

################################################################################
# Default properties of visual studio projects
################################################################################
set(DEFAULT_CXX_PROPS "${CMAKE_CURRENT_LIST_DIR}/DefaultCXX.cmake")
set(DEFAULT_Fortran_PROPS "${CMAKE_CURRENT_LIST_DIR}/DefaultFortran.cmake")
